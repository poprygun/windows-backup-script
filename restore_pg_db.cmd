@echo off

call env.cmd

set BKP_FILE=%1
set DB_NAME=%2
echo === RESTORING %DB_NAME%...
dropdb %DB_NAME%
createdb %DB_NAME%
pg_restore.exe --format=c --create --clean --dbname=%DB_NAME% --username=postgres %BKP_FILE%
echo DONE!

pause