@echo off

call env.cmd

set DB=%1
set BKP_NAME=%PREFIX%__%DB%

echo + Backing up MYSQL db %1 == %BKPDIR%\%PREFIX%\%BKP_NAME%.sql
mariadb-dump --host=%MYSQLHOST% --user=%MYSQLUSER% --password=%MYSQLPASSWORD% --port=%MYSQLPORT% --databases %DB% > %BKPDIR%\%PREFIX%\%BKP_NAME%.sql

REM Uncomment this if you need a zip dumps (it takes many time but makes files up to ~10% smaller)
echo + Zipping up file %BKPDIR%\%PREFIX%\%BKP_NAME%.sql == %BKPDIR%\%PREFIX%\%BKP_NAME%.sql.7z
7z a -bt -mx9 -sdel -slp -t7z  "%BKPDIR%\%PREFIX%\%BKP_NAME%.sql.7z" "%BKPDIR%\%PREFIX%\%BKP_NAME%.sql"

exit