@echo off


call env.cmd

set PREFIX=2022-12-07

set DB_NAME=ITS_BP_RU
set BKP_FILE=%BKPDIR%\%PREFIX%\%PREFIX%__%DB_NAME%.pgdump
start "" restore_pg_db.cmd %BKP_FILE% %DB_NAME% 

set DB_NAME=ITS_UNF_KZ
set BKP_FILE=%BKPDIR%\%PREFIX%\%PREFIX%__%DB_NAME%.pgdump
start "" restore_pg_db.cmd %BKP_FILE% %DB_NAME%

set DB_NAME=ITS_UNF_RU
set BKP_FILE=%BKPDIR%\%PREFIX%\%PREFIX%__%DB_NAME%.pgdump
start "" restore_pg_db.cmd %BKP_FILE% %DB_NAME%

pause