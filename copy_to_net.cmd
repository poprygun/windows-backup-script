@echo off

chcp 1251
call env.cmd

echo === SYNCING WITH NETWORK SHARE %NETWORK_SHARE%
net use W: %NETWORK_SHARE_DIR% %NETWORK_SHARE_PASSWORD% /USER:%NETWORK_SHARE_USER%
robocopy %BKPDIR% W: /E /R:1  /maxage:1 /MT
net use W: /DELETE

exit