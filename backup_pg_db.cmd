@echo off

call env.cmd

set DB=%1
set BKP_NAME=%PREFIX%__%DB%

echo + Backing up db %1 == %BKPDIR%\%BKP_NAME%.pgdump
pg_dump --create --format=c --file=%BKPDIR%\%PREFIX%\%BKP_NAME%.pgdump %DB%

REM Uncomment this if you need a zip dumps (it takes many time but makes files up to ~10% smaller)
REM echo + Zipping up file %BKPDIR%\%BKP_NAME%.pgdump == %BKPDIR%\%BKP_NAME%.pgdump.7z
REM 7z a -bt -mx9 -sdel -slp -t7z  "%BKPDIR%\%PREFIX%\%BKP_NAME%.pgdump.7z" "%BKPDIR%\%BKP_NAME%.pgdump"

REM echo + Deleting up dump "%BKPDIR%\%PREFIX%\%BKP_NAME%.pgdump"
REM DEL "%BKPDIR%\%PREFIX%\%BKP_NAME%.pgdump"

exit