@echo off

call env.cmd
chcp 1251

for /f %%i in (files-list.txt) do (
  echo === BACKUP FILES %%i using list-file files-list_%%i.txt
  7z.exe a -mx=5 -r -t7z -ssw -sccWIN -scsWIN -slp -spf -y -xr@exclude-list.txt %BKPDIR%\%PREFIX%\%PREFIX%_%%i.7z @files-list_%%i.txt
)

ping 127.0.0.1 -n 5 > nul
progressbar 10 "Waiting while 7z ends.." 7z.exe

exit