@echo off

call env.cmd

echo === SYNCING WITH RSYNC %NASDIR%
rsync -avz -H --backup --recursive --delete --progress --stats --exclude '.git' %BKPDIR_CYG%/* %NASDIR%