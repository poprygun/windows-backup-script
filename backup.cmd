@echo off

chcp 1251

call env.cmd
mkdir %BKPDIR%\%PREFIX%

if %DO_BACKUP_PG% EQU 1 start "" backup_pg.cmd
if %DO_BACKUP_MYSQL% EQU 1 start "" backup_mysql.cmd
if %DO_BACKUP_FILES% EQU 1 start "" backup_files.cmd

if %DO_BACKUP_PG% EQU 1 progressbar 10 "Waiting while pg_dump ends.." pg_dump.exe
if %DO_BACKUP_MYSQL% EQU 1 progressbar 10 "Waiting while mariadb-dump ends.." mariadb-dump.exe
progressbar 10 "Waiting while 7z ends.." 7z.exe

if %ERRORLEVEL% NEQ 0 exit %ERRORLEVEL%

if %DO_CLEAR_OLD% EQU 1 call clear_old_files.cmd

if %DO_COPY_TO_RSYNC% EQU 1 start "" copy_to_rsync.cmd
if %DO_COPY_TO_RSYNC% EQU 1 progressbar 10 "Waiting while rsync ends.." rsync.exe

if %DO_COPY_TO_NETWORK_SHARE% EQU 1 call copy_to_net.cmd
if %DO_COPY_TO_NETWORK_SHARE% EQU 1 progressbar 10 "Waiting while robocopy ends.." robocopy.exe

exit