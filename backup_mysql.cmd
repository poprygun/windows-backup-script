@echo off

call env.cmd

echo === GETTING MYSQL DATABASES
mysql --host=%MYSQLHOST% --user=%MYSQLUSER% --password=%MYSQLPASSWORD% --port=%MYSQLPORT%  -s -N -e "SHOW DATABASES" > dblist_mysql.txt

echo === BACKUP MYSQL START

for /f %%i in (dblist_mysql.txt) do (
  start /b "" backup_mysql_db.cmd %%i
)

ping 127.0.0.1 -n 5 > nul
del dblist_mysql.txt
progressbar 10 "Waiting while mariadb-dump ends.." mariadb-dump.exe

exit