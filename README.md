# Universal Windows backup script

This script allows you to backup any files and databases into directory and copy to rsync server

## Getting started

To use this you must:
- Clone this repo
- Rename `env.cmd.example` file into `env.cmd` 
- Set your variables in `env.cmd` file
- Remove unused backups from `backup.cmd` file
- Add into scheduler execution of `backup.cmd` file
- PROFIT!

## Roadmap
- [x] Working on any windows in portable mode
- [x] Making backups in async mode
- [x] Backup and zip PostgreSQL databases
- [x] Backup and zip MySQL databases
- [ ] Backup and zip MSSQL databases
- [ ] Backup and zip files and folders
- [ ] Making HTTP-call at the end and on error

## License
Completele open and free for using! \
Contributors are welcome!

## Author
Made by Andrey Zolotarev @ ITS.bz \
Site: https://its.bz \
Email: dev@its.bz
