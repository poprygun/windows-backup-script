@echo off

call env.cmd

REM Deleting old files in all subdirectories
forfiles /p "%BKPDIR%" /S /D -%MAX_DAYS% /C "cmd /c del /f /a /q @file"

REM Delete empty directories
:repeat
for /f "tokens=*" %%i in (' dir /b /s /ad "%BKPDIR%" ') do 2>nul rd /q "%%i" && goto:repeat
