@echo off

call env.cmd 

echo === GETTING POSTGRES DATABASES

rem create a text file listing all databases
rem adjust here if you want to exclude a database
psql -X -c "select datname from pg_database where not datistemplate" -A -t -o dblist_pg.txt -d template1

echo === BACKUP POSTGRES GLOBALS

rem dump all user accounts and roles
mkdir %BKPDIR%\%PREFIX%
"%PGBINDIR%\pg_dumpall" --globals-only --file=%BKPDIR%\%PREFIX%\%PREFIX%__postgres_globals.sql

REM Uncomment this if you need a zip dumps (it takes many time but makes files up to ~10% smaller)
REM "%ZIPDIR%\7z.exe" a "%BKPDIR%\%PREFIX%\%PREFIX%__postgres_globals.sql.zip" "%BKPDIR%\%PREFIX%__postgres_globals.sql"
REM DEL "%BKPDIR%\%PREFIX%__postgres_globals.sql"

echo === BACKUP POSTGRES START

for /f %%i in (dblist_pg.txt) do (
  start /b "" backup_pg_db.cmd %%i
)

ping 127.0.0.1 -n 5 > nul
del dblist_pg.txt
progressbar 10 "Waiting while pg_dump ends.." pg_dump.exe

exit